var xhttp = createXMLHttpRequestObject();
function createXMLHttpRequestObject(){
	var xhttp;
	if(window.XMLHttpRequest){
		xhttp = new XMLHttpRequest();
	}else{
		xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(!xhttp){
		alert("Can not create that object");
	}else{
		return xhttp;
	}
}

function process(){
	if(xhttp.readyState == 4 || xhttp.readyState == 0) {
		food = encodeURIComponent(document.getElementById("food").value);
		xhttp.open("GET", "food.php?food=" + food, true);
		xhttp.onreadystatechange = handleServerResponse;
		xhttp.send(null);
	}else{
		setTimeout('process()', 1000);
	}
}
function handleServerResponse(){
	if(xhttp.readyState == 4){
		if(xhttp.status == 200){
			xmlResponse = xhttp.responseXML;
			xmlDocumentElement = xmlResponse.documentElement;
			message = xmlDocumentElement.firstChild.data;
			document.getElementById("message").innerHTML = '<span style="color:blue">'+ message +'</span>';
			setTimeout('process()', 1000);
		}else{
			alert("something wrong");
		}
	}
}