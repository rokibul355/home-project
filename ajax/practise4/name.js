var xhttp;
if(window.XMLHttpRequest){
	xhttp = new XMLHttpRequest();
}else{
	xhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
function check(){
	var name = document.getElementById("name").value;
	var time = new Date();
	var min = time.getMinutes();
	var sec = time.getSeconds();
	if(min < 10){
		min = "0" + min;
	}
	if(sec < 10){
		sec = "0" + sec;
	}
	xhttp.open("GET", "name.php?name=" + name, true);
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState == 4 && xhttp.status == 200){
			xmlResponse = xhttp.responseXML;
			message = xmlResponse.documentElement.firstChild.data;
			document.getElementById("demo").innerHTML = "<span style='color: blue;'>" + message + "</span>";
			document.getElementById("time").innerHTML = "<span style='color: blue;'>" + "Time : " + time.getHours() +" : "+ min +" : " + sec + "</span>";
			setTimeout("check()", 1000);
		}
	}
	xhttp.send(null);
}