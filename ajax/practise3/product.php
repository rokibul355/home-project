<?php 
header('Content-Type: text/xml');
echo "<?xml version='1.0' encoding='UTF-8'?>";
echo "<response>";
$product = $_GET['product'];
$product_array = array("salt", "rice", "soap", "oil", "biscuit", "potato", "sugar", "egg");
if(in_array($product, $product_array)){
	echo "We have $product ";
}elseif($product == ""){
	echo "Please enter your product name to order.";
}else{
	echo "We don't have $product";
}
echo "</response>";
?>
