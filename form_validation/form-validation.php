<?php
	$error_message = '';
	$success_message = '';
	if ($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$valid = 1;
		if(empty($_POST['u_name'])) 
		{
			$valid = 0;
			$error_message .= "Name can not be empty <br>";
		}
		if(empty($_POST['u_email'])) 
		{
			$valid = 0;
			$error_message .= "Email Address can not be empty <br>";
		}
		else
		{
			if(filter_var($_POST['u_email'], FILTER_VALIDATE_EMAIL) === false) 
			{
				$valid = 0;
				$error_message .= "Email Address is invalid <br>";
			}
		}
		if(empty($_POST['u_gender'])) 
		{
			$valid = 0;
			$error_message .= "You must have to select a gender <br>";
		}
		if(empty($_POST['u_country'])) 
		{
			$valid = 0;
			$error_message .= "You must have to select a country <br>";
		}
		if(empty($_POST['u_bio'])) 
		{
			$valid = 0;
			$error_message .= "Bio can not be empty <br>";
		}
		if(empty($_POST['u_username'])) 
		{
			$valid = 0;
			$error_message .= "Username can not be empty <br>";
		}
		else
		{
			$len = strlen($_POST['u_username']);
			if($len < 5)
			{
				$valid = 0;
				$error_message .= "Username must be at least 5 characters in long <br>";
			}
			else
			{
				$count_number = preg_match_all( "/[0-9]/", $_POST['u_username'] );
				$count_alphabet = preg_match_all( "/[A-Za-z]/", $_POST['u_username'] );
				if( $count_number == 0 || $count_alphabet == 0 )
				{
					$valid = 0;
					$error_message .= "Username must contain at least 1 number and 1 alphabetic character <br>";
				}
			}
		}
		if(empty($_POST['u_password'])) 
		{
			$valid = 0;
			$error_message .= "Password can not be empty <br>";
		}
		if(empty($_POST['u_re_password'])) 
		{
			$valid = 0;
			$error_message .= "Retype Password can not be empty <br>";
		}
		if(!empty($_POST['u_password']) && !empty($_POST['u_re_password']))
		{
			if($_POST['u_password'] != $_POST['u_re_password'])
			{
				$valid = 0;
				$error_message .= "Passwords do not match <br>";
			}
		}

		if(empty($_POST['u_term']))
		{
			$valid = 0;
			$error_message .= "You must have to agree with the terms <br>";
		}

		if($valid == 1)
		{
			$success_message = "Everything is OK";
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP Form Validation</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="container">
		<h1>PHP Form Validation</h1>
		<?php  
			if($error_message != '')
			{
				echo '<div class="red">'.$error_message.'</div><br>';
			}
			if($success_message != '')
			{
				echo '<div class="green">'.$success_message.'</div><br>';
			}
		?>
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
			<table>
				<tr>
					<td>Name: </td>
					<td><input type="text" name="u_name"></td>
				</tr>
				<tr>
					<td>Email Address: </td>
					<td><input type="text" name="u_email"></td>
				</tr>
				<tr>
					<td>Gender: </td>
					<td>
						<input type="radio" name="u_gender"> Male
						<input type="radio" name="u_gender"> Female
					</td>
				</tr>
				<tr>
					<td>Select a Country: </td>
					<td>
						<select name="u_country">
							<option value="">Select a Country</option>
							<?php 
							$countries = file("countries.txt");
							foreach($countries as $country){
							?>	
								<option value="<?php echo $country; ?>"><?php echo $country; ?></option>
							<?php	
							}
							?>
						</select>
					</td>				
				</tr>
				<tr>
					<td>Bio: </td>
					<td>
						<textarea name="u_bio" cols="30" rows="10"></textarea>
					</td>
				</tr>
				<tr>
					<td>Username: <br><span class="blue">(Must be at least 5 characters where 1 numeric and 1 alphabetic character must be included)</span></td>
					<td><input type="text" name="u_username"></td>
				</tr>
				<tr>
					<td>Password: </td>
					<td><input type="password" name="u_password"></td>
				</tr>
				<tr>
					<td>Retype Password: </td>
					<td><input type="password" name="u_re_password"></td>
				</tr>
				<tr>
					<td>Terms: </td>
					<td>
						<input type="checkbox" name="u_term"> I agree with the terms of this page
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="SUBMIT"></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>