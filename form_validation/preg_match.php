<?php 
if(isset($_POST['form'])){
	if(empty($_POST['password'])){
		echo "Password can not be empty!";
	}else{
		$length = strlen($_POST['password']);
		if($length<8){
			echo "Password must be at least 8 characters!";
		}else{
			$number = preg_match("/[0-9]/", $_POST['password']);
			$upper_char = preg_match("/[A-Z]/", $_POST['password']);
			if($number == 0 || $upper_char == 0){
				echo "Password must be 1 number and 1 upper character!";
			}else{
				echo "Password Saved successfully !";
			}
		}
	}
}
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Password field</title>
</head>
<body>
	<form action="" method="post">
		<table>
			<tr>
				<td>Password</td>
				<td><input type="password" name="password"/></td>
				<td>Password must be at least 8 character and 1 number 1 upper character</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="form" value="Save"/></td>
			</tr>
		</table>
	</form>
</body>
</html>
