var xhttp;
if(window.XMLHttpRequest){
	xhttp = new XMLHttpRequest();
}else{
	xhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
function check(){
	if(xhttp.readyState == 4 || xhttp.readyState == 0) {
		email = encodeURIComponent(document.getElementById("email").value);
		xhttp.open("GET", "check.php?email=" + email, true);
		xhttp.onreadystatechange = handleServerResponse;
		xhttp.send(null);
	}else{
		setTimeout('check()', 1000);
	}
}
function handleServerResponse(){
	if(xhttp.readyState == 4){
		if(xhttp.status == 200){
			xmlResponse = xhttp.responseXML;
			alert(xmlResponse);
			xmlDocumentElement = xmlResponse.documentElement;
			message = xmlDocumentElement.firstChild.data;
			document.getElementById("check").innerHTML = message;
			setTimeout('check()', 1000);
		}else{
			alert("something wrong");
		}
	}
}