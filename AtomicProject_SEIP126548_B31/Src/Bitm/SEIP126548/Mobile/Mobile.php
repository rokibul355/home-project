<?php
namespace App\Bitm\SEIP126548\Mobile;
use App\Bitm\SEIP126548\Message\Message;
use PDO;
class Mobile
{
    public $id = "";
    public $title = "";
    public $model_name = "";
    public $created = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";
    public $connection = "";
    public $host = "localhost";
    public $db_name = "atomic_project";
    public $user = "root";
    public $pass = "";

    public function __construct()
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($data){
        if(array_key_exists("title", $data) && !empty($data)){
            $this->title = $data['title'];
        }
        if(array_key_exists("model_name", $data) && !empty($data)){
            $this->model_name = $data['model_name'];
        }
        if(array_key_exists("id", $data) && !empty($data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function store()
    {
		$result = $this->index();
		$duplicate = 0;
		foreach($result as $row){
			if($row['title'] == $this->title && $row['model_name'] == $this->model_name){
				$duplicate = 1;
			}
		}
		if($duplicate == 1){
			Message::message("<div class='alert alert-danger'>This model is already inserted.</div>");
			header("location: create.php");
		}else{
        $statement = $this->connection->prepare("insert into student (title, model_name) values (:title, :model)");
        $result = $statement->execute(array("title"=>$this->title, "model"=>$this->model_name));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been inserted successfully.</div>");
			header("location: index.php");
		}else{
            Message::message("<div class='alert alert-danger'>Data has not been inserted successfully.</div>");
            header("location: create.php");
        }
	}
    }
    public function index()
    {
        $statement = $this->connection->query("select * from student");
		$_all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $_all_data;
    }

    public function view(){
        $statement = $this->connection->prepare("select * from student where id=:id");
        $statement->execute(array("id"=>$this->id));
        $single_data = $statement->fetch(PDO::FETCH_ASSOC);
        return $single_data;
    }

    public function create()
    {
        return "I am create form<br>";
    }



    public function edit()
    {
        return "I am editing form<br>";
    }

    public function update()
    {
        return "I am updating data<br>";
    }

    public function delete()
    {
        return "I delete data";
    }

}
?>