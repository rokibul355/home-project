<?php 
class A{
	public $name = "Rokibul";
	public function info(){
		if(isset($this)){
			echo '$this is defined in class <br>';
			echo get_class($this);
		}else{
			echo '$this is not defined<br>';
		}
	}
}
class B{
	public function bar(){
		A::info();
	}
}
A::info();
$obj = new A();
$obj->info();
B::bar();
$obj2 = new B();
$obj2->bar();
?>