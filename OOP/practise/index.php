<?php 
function __autoload($name){
	$array = explode("\\", $name);
	array_shift($array);
	$file = "Src/".implode("/", $array).".php";
	include($file);
}
use App\Modules\Hello\Hello;
use App\Modules\Welcome\Welcome;
$obj = new Hello();
$obj->hi();
echo "<br>";
$obj2 = new Welcome();
$obj2->message();
?>