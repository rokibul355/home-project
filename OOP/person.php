<?php 
class person{
	const name = "Rokibul";
	public function __construct($name){
		$this->name = $name;
	}
	public function dance(){
		
	}
}
$me = new person("Rokibul");
if(is_a($me, "person")){
	echo "My name is ".$me->name;
}
if(property_exists($me, "name")){
	echo " I have a name";
}
if(method_exists($me, "dance")){
	echo " and I can't dance";
}
?>