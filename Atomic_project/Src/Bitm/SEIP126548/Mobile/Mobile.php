<?php 
namespace App\Bitm\SEIP126548\Mobile;
use PDO;
class Mobile{
	public $id = "";
	public $title = "";
	public $created = "";
	public $modified = "";
	public $created_by = "";
	public $modified_by = "";
	public $deleted_at = "";
	public $connection = "";
	public $database_name = "atomic_project";
	public $host = "localhost";
	public $user = "root";
	public $password = "";
	public function __construct(){
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->database_name", $this->user, $this->password);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	public function setData($data=""){
		if(array_key_exists("model_name", $data) && !empty($data)){
			$this->title = $data['model_name'];
		}
		return $this;
	}
	public function store(){
		$statement = $this->connection->prepare("INSERT INTO mobile_model (model_name) value (:model_name)");
		$result = $statement->execute(array("model_name"=>$this->title));
		
		var_dump($result);
	}
	public function view(){
		$statement = $this->connection->prepare("select * from mobile_model order by model_name asc");
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}
?>