<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit09d7a3ee8132f9069b5d546dff3ef052
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit09d7a3ee8132f9069b5d546dff3ef052::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit09d7a3ee8132f9069b5d546dff3ef052::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
