<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Javascript</title>
	<script type="text/javascript">
		function changePara(){
			document.getElementById("para").innerHTML = "Wow I changed the paragraph successfully !";
		}
	</script>
</head>
<body>
	<button type="button" onclick="changePara()">Change the below paragraph</button>
	<p id="para">This paragraph will be change.</p>
</body>
</html>