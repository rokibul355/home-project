<?php 
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Combine\Combine;
$obj = new Combine();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Combine Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<h3>Add Information Form</h3>
			</div>
			<div class="col-md-4"></div>
		</div>
		<form action="store.php" method="post">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 form-group">
					<label for="name">Name </label>
					<input type="text" id="name" name="name" required="required" autofocus="autofocus" class="form-control" placeholder="Enter your name"/>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 form-group">
					<label for="email">Email </label>
					<input type="email" id="email" name="email" required="required" class="form-control" placeholder="Enter your email"/>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 form-group">
					<label for="mobile_model">Favourite mobile model </label>
					<input type="text" id="mobile_model" name="mobile_model" required="required" class="form-control" placeholder="Enter your favourite mobile model"/>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="">Select your gender :</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-2 form-group col-md-offset-2">
					<label for="male" class="radio-inline"><input type="radio" value="Male" name="gender" id="male"/>Male</label>
				</div>
				<div class="col-md-6 form-group">
					<label for="female" class="radio-inline"><input type="radio" value="Female" name="gender" id="female"/>Female</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="">Select your hobbies :</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row"> 
				<div class="col-md-4"></div>
				<div class="col-md-6 form-group">
					<div class="scroll" style="overflow-x: hidden; overflow-y: scroll; height:200px; border: 1px solid #C0C0C0;">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="coding"><input type="checkbox" name="hobbies[]" id="coding" value="Coding"/>Coding</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="gardening"><input type="checkbox" name="hobbies[]" id="gardening" value="Gardening"/>Gardening</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="cricket"><input type="checkbox" name="hobbies[]" id="cricket" value="Cricket"/>Cricket</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="fighting"><input type="checkbox" name="hobbies[]" id="fighting" value="Fighting"/>Fighting</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="movie"><input type="checkbox" name="hobbies[]" id="movie" value="Watching Movie"/>Watching  Movie</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="driving"><input type="checkbox" name="hobbies[]" id="driving" value="Driving"/>Driving</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="dancing"><input type="checkbox" name="hobbies[]" id="dancing" value="Dancing"/>Dancing</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="travelling"><input type="checkbox" name="hobbies[]" id="travelling" value="Travelling"/>Travelling</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="singing"><input type="checkbox" name="hobbies[]" id="singing" value="Singing"/>Singing</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="wrestling"><input type="checkbox" name="hobbies[]" id="wrestling" value="Wrestling"/>Wrestling</label>
							</div>
							<div class="col-md-4"></div>
						</div>
					</div>
				</div>	
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-9"></div>
				<div class="col-md-1">
					<input type="submit" value="Submit" class="btn btn-success"/>
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>
		
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>