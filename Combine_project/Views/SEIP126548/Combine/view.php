<?php 
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Combine\Combine;
$obj = new Combine();
$obj->setData($_GET);
$data = $obj->view();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Combine Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 col-md-offset-1">
				<h3>Details Information</h3>
			</div>
			<div class="col-md-4"></div>
		</div>
		
		
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<ul class="list-group">
					<li class="list-group-item list-group-item-info"><span style="color: #31B0D5; font-weight: bold">ID : </span><?php echo $data['id'];?></li>
					<li class="list-group-item list-group-item-info"><span style="color: #31B0D5;font-weight: bold">Name : </span><?php echo $data['name'];?></li>
					<li class="list-group-item list-group-item-info"><span style="color: #31B0D5;font-weight: bold">Email : </span><?php echo $data['email'];?></li>
					<li class="list-group-item list-group-item-info"><span style="color: #31B0D5;font-weight:bold">Favourite model : </span><?php echo $data['mobile_model'];?></li>
					<li class="list-group-item list-group-item-info"><span style="color: #31B0D5;font-weight: bold">Geder : </span><?php echo $data['gender'];?></li>
					<li class="list-group-item list-group-item-info"><span style="color: #31B0D5;font-weight: bold">Hobbies : </span><?php echo $data['hobbies'];?></li>
				</ul>
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script type="text/javascript"> 
	$("#message").show().delay(3000).fadeOut();
	</script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>