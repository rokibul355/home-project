<?php 
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Combine\Combine;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Combine();
$obj->setData($_GET);
$data = $obj->view();
$var = explode(",", $data['hobbies']);
$data['hobbies'] = $var;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Combine Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<h3>Edit Information</h3>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" id="message">
				<?php 
				if(array_key_exists("message", $_SESSION) && !empty($_SESSION['message'])){
					echo Message::message();
				}
				?>
			</div>
			<div class="col-md-2"></div>
		</div>
		<form action="update.php" method="post">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 form-group">
					<label for="name">Name </label>
					<input type="hidden" name="id" value="<?php echo $data['id'];?>"/>
					<input type="text" id="name" name="name" value="<?php echo $data['name'];?>" required="required" class="form-control" placeholder="Enter your name"/>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 form-group">
					<label for="email">Email </label>
					<input type="email" id="email" name="email" required="required" value="<?php echo $data['email'];?>" class="form-control" placeholder="Enter your email"/>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 form-group">
					<label for="mobile_model">Favourite mobile model </label>
					<input type="text" id="mobile_model" name="mobile_model" required="required" value="<?php echo $data['mobile_model'];?>" class="form-control" placeholder="Enter your favourite mobile model"/>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="">Select your gender :</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-2 form-group col-md-offset-2">
					<label for="male" class="radio-inline"><input type="radio" value="Male" name="gender" id="male" <?php if(in_array("Male", $data)){echo "checked";}?>/>Male</label>
				</div>
				<div class="col-md-6 form-group">
					<label for="female" class="radio-inline"><input type="radio" value="Female" name="gender" id="female" <?php if(in_array("Female", $data)){echo "checked";}?>/>Female</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<label for="">Select your hobbies :</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row"> 
				<div class="col-md-4"></div>
				<div class="col-md-6 form-group">
					<div class="scroll" style="overflow-x: hidden; overflow-y: scroll; height:200px; border: 1px solid #C0C0C0;">
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="coding"><input type="checkbox" name="hobbies[]" id="coding" value="Coding" <?php if(in_array("Coding", $data['hobbies'])){echo "checked";}?>/>Coding</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="gardening"><input type="checkbox" name="hobbies[]" id="gardening" value="Gardening" <?php if(in_array("Gardening", $data['hobbies'])){echo "checked";}?>/>Gardening</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="cricket"><input type="checkbox" name="hobbies[]" id="cricket" value="Cricket" <?php if(in_array("Cricket", $data['hobbies'])){echo "checked";}?>/>Cricket</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="fighting"><input type="checkbox" name="hobbies[]" id="fighting" value="Fighting" <?php if(in_array("Fighting", $data['hobbies'])){echo "checked";}?>/>Fighting</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="movie"><input type="checkbox" name="hobbies[]" id="movie" value="Watching Movie" <?php if(in_array("Watching Movie", $data['hobbies'])){echo "checked";}?>/>Watching  Movie</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="driving"><input type="checkbox" name="hobbies[]" id="driving" value="Driving" <?php if(in_array("Driving", $data['hobbies'])){echo "checked";}?>/>Driving</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="dancing"><input type="checkbox" name="hobbies[]" id="dancing" value="Dancing" <?php if(in_array("Dancing", $data['hobbies'])){echo "checked";}?>/>Dancing</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="travelling"><input type="checkbox" name="hobbies[]" id="travelling" value="Travelling" <?php if(in_array("Travelling", $data['hobbies'])){echo "checked";}?>/>Travelling</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="singing"><input type="checkbox" name="hobbies[]" id="singing" value="Singing" <?php if(in_array("Singing", $data['hobbies'])){echo "checked";}?>/>Singing</label>
							</div>
							<div class="col-md-4"></div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<label class="checkbox-inline" for="wrestling"><input type="checkbox" name="hobbies[]" id="wrestling" value="Wrestling" <?php if(in_array("Wrestling", $data['hobbies'])){echo "checked";}?>/>Wrestling</label>
							</div>
							<div class="col-md-4"></div>
						</div>
					</div>
				</div>	
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-9"></div>
				<div class="col-md-1">
					<input type="submit" value="Update" class="btn btn-success"/>
				</div>
				<div class="col-md-2"></div>
			</div>
		</form>
		
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript"> 
	$("#message").show().delay(3000).fadeOut();
	</script>
  </body>
</html>