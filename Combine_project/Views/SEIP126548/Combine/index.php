<?php 
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Combine\Combine;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Combine();
$all_data = $obj->index();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Combine Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 col-md-offset-1">
				<h3>All List</h3>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-1">
				
			</div>
			<div class="col-md-7">
				<a href="create.php" class="btn btn-success">Create New</a>
				<a href="trashed.php" class="btn btn-primary">Trashed Record</a><br><br>
			</div>
			<div class="col-md-4"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10" id="message">
				<?php 
					if(array_key_exists("message", $_SESSION) && !empty($_SESSION['message'])){
						echo Message::message();
					}
				?>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>SL</th>
							<th>ID</th>
							<th>Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						foreach($all_data as $data){
							$i++;
						?>
						<tr>
							<td><?php echo $i; ?></td>
							<td><?php echo $data['id'];?></td>
							<td><?php echo $data['name'];?></td>
							<td width="21%">
								<a href="view.php?id=<?php echo $data['id'];?>" class="btn btn-success">View</a>
								<a href="edit.php?id=<?php echo $data['id'];?>" class="btn btn-info">Edit</a>
								<a href="trash.php?id=<?php echo $data['id'];?>" class="btn btn-warning" onclick="return confirm_delete();">Delete</a>
							</td>
						</tr>
						<?php 
						}
						?>
					</tbody>
				</table>	
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript"> 
	$("#message").show().delay(3000).fadeOut();
	function confirm_delete(){
		var ok = confirm("Are you sure want to delete this record?");
		if(ok){
			return true;
		}else{
			return false;
		}
	}
	</script>
  </body>
</html>