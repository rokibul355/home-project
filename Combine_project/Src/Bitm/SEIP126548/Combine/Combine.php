<?php 
namespace App\Bitm\SEIP126548\Combine;
use App\Bitm\SEIP126548\Message\Message;
use PDO;
class Combine{
	public $id = "";
	public $name = "";
	public $mobile_model = "";
	public $email = "";
	public $hobbies = "";
	public $gender = "";
	public $connection = "";
	public $host = "localhost";
	public $db_name = "combine_project";
	public $user = "root";
	public $pass = "";
	
	public function __construct(){
		try{
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	public function setData($data = ""){
		if(array_key_exists("name", $data) && !empty($data)){
			$this->name = $data['name'];
		}
		if(array_key_exists("email", $data) && !empty($data)){
			$this->email = $data['email'];
		}
		if(array_key_exists("mobile_model", $data) && !empty($data)){
			$this->mobile_model = $data['mobile_model'];
		}
		if(array_key_exists("gender", $data) && !empty($data)){
			$this->gender = $data['gender'];
		}
		if(array_key_exists("hobbies", $data) && !empty($data)){
			$this->hobbies = $data['hobbies'];
		}
		if(array_key_exists("id", $data) && !empty($data)){
			$this->id = $data['id'];
		}
		return $this;
	}
	public function store(){
		$statement = $this->connection->prepare("insert into user_information (name, email, mobile_model, gender, hobbies) values (:name, :email, :model, :gender, :hobbies)");
		$result = $statement->execute(array("name"=>$this->name, "email"=>$this->email, "model"=>$this->mobile_model, "gender"=>$this->gender, "hobbies"=>$this->hobbies));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been stored successfully!</div>");
			header("location: index.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been stored successfully!</div>");
			header("location: create.php");
		}
	}
	public function index(){
		$statement = $this->connection->query("select * from user_information where deleted_at is null");
		$all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $all_data;
	}
	public function view(){
		$statement = $this->connection->prepare("select * from user_information where id = :id");
		$statement->execute(array("id"=>$this->id));
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		return $data;
	}
	public function update(){
		if($this->hobbies == ""){
			$hobby_empty = 1;
		}
		if($hobby_empty == 1){
			Message::message("<div class='alert alert-danger'>Please select your hobbies first!</div>");
			header("location: edit.php?id=$this->id");
		}else{
		$statement = $this->connection->prepare("update combine_project.user_information set name=:name, email=:email, mobile_model=:model, gender=:gender, hobbies=:hobbies where user_information.id = :id");
		$result = $statement->execute(array("name"=>$this->name, "email"=>$this->email, "model"=>$this->mobile_model, "gender"=>$this->gender, "hobbies"=>$this->hobbies, "id"=>$this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been updated successfully!</div>");
			header("location: index.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been updated successfully!</div>");
			header("location: index.php");
		}
		}
	}
	public function trash(){
		date_default_timezone_set("Asia/Dhaka");
		$this->deleted_at = date("d-m-Y h:i:s A");
		$statement = $this->connection->prepare("update user_information set deleted_at = :date where id = :id");
		$result = $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been deleted successfully!</div>");
			header("location: index.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been deleted successfully!</div>");
			header("location: index.php");
		}
	}
	public function trashView(){
		$statement = $this->connection->query("select * from user_information where deleted_at is not null");
		$all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $all_data;
	}
	public function delete(){
		$statement = $this->connection->prepare("delete from user_information where id = :id");
		$result = $statement->execute(array("id"=>$this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been deleted successfully!</div>");
			header("location: trashed.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been deleted successfully!</div>");
			header("location: trashed.php");
		}
	}
	public function restore(){
		$this->deleted_at = null;
		$statement = $this->connection->prepare("update user_information set deleted_at = :date where id = :id");
		$result = $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
		if($result){
			Message::message("<div class='alert alert-success'>Data has been restored successfully!</div>");
			header("location: trashed.php");
		}else{
			Message::message("<div class='alert alert-danger'>Data has not been restored successfully!</div>");
			header("location: trashed.php");
		}
	}
}
?>