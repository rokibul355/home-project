<?php 
$username = "rokibul";
$email = "rokibultct@gmail.com";
$password = "admin";
function session(){
	session_start();
	$_SESSION['name'] = "shakil";
	header("location: index.php");
}
if(isset($_COOKIE['name'])){
	session();
}
if(isset($_POST['form'])){
	$my_user = $_POST['user_email'];
	$my_email = $_POST['user_email'];
	$my_pass = $_POST['password'];
	try{
		if((($my_user == $username && $my_pass == $password) || ($my_email == $email && $my_pass == $password)) && (isset($_POST['check']))){
			setcookie("name", "my_cookie", time()+(60*60));
			session();
		}elseif(($my_user == $username && $my_pass == $password) || ($my_email == $email && $my_pass == $password)){
			session();
		}else{
			throw new Exception("Username/Email or Password invalid.");
		}
	}
	catch(Exception $e){
		$error_message = $e->getMessage();
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/form.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script type="text/javascript">
		function showPass(){
			if(document.getElementById('check').checked){
				document.getElementById('pass').type='text';
			}else{
				document.getElementById('pass').type='password';
			}
		}
	</script>
  </head>
  <body>
	<section class="container background">
		<div class="row">
			<div class="col-md-5"></div>
			<div class="col-md-7">
				<h2>Login Form</h2>
			</div>
		</div>
		
		<form action="" method="post" autocomplete="on">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">	
						<label for="name">Username / Email :</label>
						<input type="text" name="user_email" id="name"class="form-control" autofocus="autofocus" required="required"/>
						
					</div>
					<div class="col-md-4">
						
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4 ">	
						
						<label for="pass">Password: </label>
						<input type="password" name="password" id="pass" class="form-control" required="required" />
						
						
						
					</div>
					<div class="col-md-4">
						<?php 
							if(isset($error_message)){
								echo $error_message;
							}
						?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4 ">	
						<div class="checkbox col-md-offset-1">
							<label for=""><input type="checkbox" id="check" onchange="showPass();"/>Show Password</label>
							
						</div>
						<div class="checkbox col-md-offset-1">
							<label for=""><input type="checkbox" name="check" />Remember me</label>
							
						</div>
						
						<input type="submit" name="form" value="Log in" class="col-md-offset-9 btn btn-primary"/>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
						
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">	
					
				</div>
				<div class="col-md-4"></div>
			</div>
		</form>
	</section>
	
		
		
    
	
	

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

